;; ------------ typewriter-mode.el ------------ ;;
;; Created:     2019-04-12, 10:37    @toshiba
;; Last update: 2019-04-12, 22:25:10 @toshiba
;; Doc:         Major mode for typewriting like in-Emacs presentations
;; Todo:        12/04, count entries, add kbd to check number at point
;;              12/04, add possibility to start counting from nth
;;              in `tr/typewriter-series'
;;
;; Code starts here:
(defgroup typewriter-mode nil
  "`typewriter-mode' custom group"
  :group 'typewriter
  :prefix "trm/")

(defface trm/code-face
  '((t :foreground "#EC407A"))
  "Face for `trm/cmd-string'"
  :group 'typewriter)

(defconst trm/cmd-string "<@>"
  "Symbol opening code line in `typewriter-mode'")

(defconst trm/keywords
  `(("^#.*$" . 'font-lock-comment-face)
    (,(concat "^" trm/cmd-string " ") ;; matcher
     (,trm/cmd-string (beginning-of-line) nil (0 'trm/code-face))
     (" [A-Z]+" (beginning-of-line) nil (0 'font-lock-function-name-face))
     ("[0-9]+" (beginning-of-line) nil (0 'font-lock-keyword-face))
     ("(\\|)" (beginning-of-line) nil (0 'font-lock-constant-face))
     ("{\\|}" (beginning-of-line) nil (0 'font-lock-variable-name-face)))
    (,(concat "^" trm/cmd-string ".*{")
     ("\\\"[*A-Za-z0-9!?;_./ -]*\\\"" (beginning-of-line) nil
      (0 'font-lock-string-face)))
    (,(concat "^" trm/cmd-string ".*(")
     ("[a-zA-Z0-9-+!?/]+" (beginning-of-line) nil (0 'font-lock-constant-face))))
  "`typewriter-mode' syntax highlighting")

(defvar typewriter-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "M-<RET>") #'trm/open-code-line)
    map)
  "Keymap for TYPEWRITER major mode")

(defun trm/open-code-line ()
  (interactive)
  (unless (eq (point) (line-beginning-position))
    (newline 1))
  (insert (format "%s " trm/cmd-string)))

(define-derived-mode typewriter-mode text-mode "Typewriter"
  "Major mode for editing *typewriter* buffer"
  (setq font-lock-defaults '(trm/keywords))
  ;; using `newcomment.el'
  (setq-local comment-start "# ")
  (setq-local comment-end ""))

(provide 'typewriter-mode)

;; typewriter-mode.el ends here
