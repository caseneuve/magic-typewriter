;; ------------ typewriter.el ------------ ;;
;; Created:     2019-04-11, 22:54    @toshiba
;; Last update: 2019-04-12, 18:19:01 @toshiba
;; Doc:         Simple typewriting machine
;;              (inspired by Howard Abrams' presentation)
;; Todos:       12/04, make major mode for tr/buf (syntax
;;                     hi-light, kbd etc.)
;;
;;
;; Code starts here:
(require 'typewriter-mode)

(defgroup typewriter nil
  "Typewriter "
  :group 'typewriter
  :prefix "tr/")

(defcustom tr/hunks nil
  "List of strings to feed typewriter"
  :group 'typewriter)

(defcustom tr/buf "*typewriter*"
  "Default `typewriter' buffer"
  :group 'typewriter)

(defcustom tr/newline? t
  "If non-nil typewriter will append newline to inserted text"
  :group 'typewriter)

(defcustom tr/literal? nil
  "If non-nil text will not be indented according to mode"
  :group 'typewriter)

(defvar tr/seq 0
  "Which hunk to insert")

(defcustom tr/commands
  '(("RET" . newline)
    ("IND" . indent-according-to-mode)
    ("MODE" . "")
    ("S" . save-buffer)
    ("M" . message)
    ("BO" . switch-to-buffer-other-window)
    ("FF" . find-file-other-window)
    ("FC" . forward-char)
    ("BC" . backward-char)
    ("FW" . forward-word)
    ("BW" . backward-word)
    ("DR" . delete-region)
    ("BOL" . beginning-of-line)
    ("EOL" . end-of-line)
    ("PL" . previous-line)
    ("NL" . next-line)
    ("KL" . kill-line)
    ("MIN" . (lambda () (goto-char (point-min))))
    ("MAX" . (lambda () (goto-char (point-max))))
    ("OL" . open-line)
    ("SET" . setq)
    )
  "Assoc list with command shortcut as KEY and command as VALUE"
  :group 'typewriter)

(defun tr/typewriter--insert (str)
  "Insert STR into the current buffer as if you were typing it by hand."
  ;; (interactive "s")
  (if (dolist
          (ch (split-string str "" t) t)
        (insert ch)
        (sit-for (/ 1.0 (+ 10 (random 80))) nil))
      (progn (unless tr/literal?
               (indent-according-to-mode))
             t)
    nil))

(defun tr/typewriter--buffer-string ()
  (let ((buf "*typewriter*"))
    (if (get-buffer buf)
        (with-current-buffer buf
          (split-string
           (buffer-substring-no-properties
            (point-min)
            (point-max))
           "\n" t "^#.*$"))
      nil)))

(defun tr/typewriter--action (str)
  (let (commands)
    (if (string= (substring str 0 3) trm/cmd-string)
        (progn
          (setq commands
                (split-string str ";" t
                              (concat trm/cmd-string " \\| ")))
          (dolist (cmd commands t)
            (tr/typewriter--eval cmd)))
      (tr/typewriter--insert
       (concat str (if tr/newline? "\n" ""))))))

(defun tr/typewriter--decode (str)
  (let (cmd args rest)
    (string-match "\\([A-Z]*\\)\\(.*\\)" str)
    (list (match-string 1 str)
          (replace-regexp-in-string "{\\|}" " " (match-string 2 str)))))

(defun tr/typewriter--eval (str)
  (let ((sexp (tr/typewriter--decode str)))
    (eval (read
           (concat "("
                   (format "%s " (assoc-default (car sexp) tr/commands))
                   (format "%s " (mapconcat 'identity (cdr sexp) " "))
                   ")")))
    ;; (message
    ;;  "%s"
    ;;  (read
    ;;   (concat "("
    ;;           (format "%s " (assoc-default (car sexp) tr/commands))
    ;;           (format "%s " (mapconcat 'identity (cdr sexp) " "))
    ;;           ")")))
    ))

(defun tr/typewriter-buffer-dwim ()
  (interactive)
  (unless (get-buffer tr/buf)
    (setq tr/seq 0))
  (switch-to-buffer-other-window tr/buf)
  (unless (eq major-mode 'typewriter-mode)
    (typewriter-mode)))

(defun tr/typewriter-series (p)
  (interactive "P")
  (let ((bufhunks (tr/typewriter--buffer-string)))
    (if p
        (setq tr/seq 0)
      (when bufhunks
        (setq tr/hunks bufhunks)))
    (if (>= tr/seq (length tr/hunks))
        (progn
          (message "No more text to type!")
          (setq tr/seq 0))
      (when (tr/typewriter--action
             (nth tr/seq tr/hunks))
        (setq tr/seq (1+ tr/seq))))))

(global-set-key (kbd "M-<f12>") #'tr/typewriter-series)

(provide 'typewriter)

;; typewriter.el ends here

;; # Example input to *typewriter* buffer:
;; <@> FF{"/tmp/test.go"}; MODE{go-mode}; SET{tr/newline? nil};
;; package main
;; <@> RET2
;; func main() {}
;; <@> BC1; RET; OL1; IND
;; txt := []string{"I", "need", "some", "packages", "to", "run!"}
;; <@> RET; IND
;; msg := strings.Join(txt, " ")
;;   // "strings" is a package to declare
;; <@> RET; IND
;; fmt.Printl(msg)
;;     // "fmt" is a package too
;; <@> RET; IND
;; // time for magic (save file)...
;; <@> S; RET; IND
;; // nice!
;; <@> RET; IND
;; // but what about errors?
;; <@> RET; IND
;; badguy := imfrom.NonExistent().Package
;; <@> RET; IND
;; // save file...
;; <@> S; RET; IND
;; // ok, let's change some code
;; <@> RET; IND
;; // and watch.
;; ...
;;
